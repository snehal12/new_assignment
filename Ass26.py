import functools


def max_in_list(llist):
    print("The max no. in list : ", end="")
    print(functools.reduce(lambda a, b: a if a > b else b, llist))


llist=[]
n=int(input("size of list"))
for i in range(0,n):
    m=int(input())
    llist.append(m)
max_in_list(llist)